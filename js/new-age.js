(function($) {
  "use strict"; // Start of use strict

  // Validate input of sessioncode 
  $("#sitzungscode").on("change", function () {
    var code = $(this).val().trim();
    // if code length is below 8 add invalid class
    if ((code.length && code.length !== 8) || isNaN(code))
      $(this).removeClass("is-valid").addClass("is-invalid");
    else if (code.length == 8) // if code length is 8 add is-valid class
      $(this).removeClass("is-invalid").addClass("is-valid");
    else $(this).removeClass("is-invalid is-valid"); 
  });
  // open frag.jetzt with session code
  $("#openSessionButton").on("click", function (event) {
    event.preventDefault();
   var fjCode = $("#sitzungscode").val() 
   // check if session code is valid
    
    if (fjCode.length == 8){
      var url = "https://frag.jetzt/participant/room/" + fjCode;
      window.location.href = url;
    }
     
  });

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 48)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict
